test:
	python3 -m pytest -vvv --mypy --mypy-ignore-missing-imports --flake8 --cov anomalus --cov-branch tests/test_*.py
