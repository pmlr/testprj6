import abc

from datetime import datetime
from logging import getLogger
from typing import List

from anomalus.post import Post


LOGGER = getLogger(__name__)


class DatabaseAdapter(metaclass=abc.ABCMeta):
    """
    Abstract class to be extended and implement database operations.
    """

    @abc.abstractmethod
    def insert_post(self, content: str, author: str, timestamp: datetime):
        """
        Insert post data into your database.
        """

    @abc.abstractmethod
    def find_posts(self, content: str) -> List[Post]:
        """
        Retrieve posts from your database with the chosen content.
        """

    @abc.abstractmethod
    def insert_edge(self, name: str, source: str, target: str):
        """
        Creates a Edge into the database.
        """

    @abc.abstractmethod
    def edge_exists(self, name: str, source: str, target: str) -> bool:
        """
        Checks if the edge exists.
        """


class DB:
    REPOST_EDGE = 'repost'

    def __init__(self, dbadapter: DatabaseAdapter):
        """
        Database access layer helper.

        :param dbadapter: Database persistence implementation.
        """
        self._adapter = dbadapter

    def save_post(self, post: Post):
        """
        Save a class:`anomalus.post.Post` instance into the database. This
        method doesn't validate if the post already exists in the database
        or not.

        :param post: New post to be saved
        """
        self._adapter.insert_post(post.content, post.author, post.timestamp)
        if post.original_author:
            if not self._adapter.edge_exists(self.REPOST_EDGE,
                                             post.author,
                                             post.original_author):
                self._adapter.insert_edge(self.REPOST_EDGE,
                                          post.author,
                                          post.original_author)
            else:
                LOGGER.debug(f'Edge {self.REPOST_EDGE} '
                             f'already exists between {post.author} '
                             f'and {post.original_author}')

    def check_for_anomaly(self, post: Post) -> bool:
        """
        Checks if a post has anomalies. Definition of anomaly:

        - Another post with same content
        - Similar post's author doesn't "repost" original post content.

        :param post: Newest post
        """
        if post.original_author is None:
            for similar in self._adapter.find_posts(content=post.content):
                if similar.author != post.author:
                    if not self._adapter.edge_exists(self.REPOST_EDGE,
                                                     similar.author,
                                                     post.author):
                        return True
        return False
