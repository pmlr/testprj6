from logging import getLogger
from typing import Dict


LOGGER = getLogger(__name__)


POST_ANOMALY = 'post-anomaly'


def push_event(event: str, info: Dict[str, str]):
    LOGGER.debug(f'New event: {event} {info}')
