from typing import NamedTuple, Optional

from datetime import datetime


class Post(NamedTuple):
    content: str
    author: str
    timestamp: datetime
    original_author: Optional[str] = None
