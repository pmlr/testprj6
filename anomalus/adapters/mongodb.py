from datetime import datetime
from typing import List

from mongoengine import Document, StringField, connect, DateTimeField

from anomalus.post import Post


class PostDoc(Document):
    """ Post Document model for schema validation. """
    content = StringField(required=True)
    author = StringField(required=True)
    original_author = StringField()
    timestamp = DateTimeField(required=True)


class EdgeDoc(Document):
    """ Edge Document model for schema validation. """
    name = StringField(required=True)
    source = StringField(required=True)
    target = StringField(required=True)
    meta = {
        'indexes': [
            {'fields': ('name', 'source', 'target'), 'unique': True}
        ]
    }


class MongoAdapter:

    @staticmethod
    def _doc2post(doc) -> Post:
        """
        Converts a :class:`PostDoc` instance into a class:`anomalus.post.Post`

        :return: The post representation used outside mongo layer.
        """
        return Post(content=doc.content,
                    author=doc.author,
                    original_author=doc.original_author,
                    timestamp=doc.timestamp)

    def __init__(self, uri: str):
        """
        Connects to a Mongo database instance.

        :param uri: Database URI
        """
        self.conn = connect(host=uri)

    def drop(self):
        """
        Drop all collections.

        CAUTION: Method used for testing purposes.
        """
        PostDoc.drop_collection()
        EdgeDoc.drop_collection()

    def insert_post(self, content: str, author: str, timestamp: datetime):
        """ Inserts a new Post Document into the collection """
        PostDoc(content=content, author=author, timestamp=timestamp).save()

    def find_posts(self, content: str) -> List[Post]:
        """ Find Post Documents  containing the certain content """
        docs = PostDoc.objects(content=content)
        return [MongoAdapter._doc2post(doc) for doc in docs]

    def insert_edge(self, name: str, source: str, target: str):
        """ Inserts a new edge into the database """
        EdgeDoc(name=name, source=source, target=target).save()

    def edge_exists(self, name: str, source: str, target: str) -> bool:
        """ Checks if a edge exists """
        return len(EdgeDoc.objects(name=name,
                                   source=source,
                                   target=target)) > 0
