from os import environ

MONGODB_URI = environ.get('MONGODB_URI',
                          'mongodb://localhost:19017/dev')
