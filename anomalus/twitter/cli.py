import asyncio
import argparse
import itertools
import sys

from typing import List

from anomalus.adapters.mongodb import MongoAdapter
from anomalus.database import DB
from anomalus.twitter.tweet import TwitterApiStatusPost, tweet_generator
from anomalus import events
from anomalus import settings


class GetTweetsCommad:

    def __init__(self, args: List[str] = sys.argv):
        """
        Instances a new commnad line handler for anomalus-get-tweets command.

        :param args: Commmand line arguments.
        """
        self.parser = argparse.ArgumentParser(args)

        self.parser.add_argument('number_of_tweets',
                                 type=int,
                                 help='Number of tweets')

        adapter = MongoAdapter(settings.MONGODB_URI)

        self.db = DB(adapter)

        self.loop = asyncio.get_event_loop()

    async def push_event(self, name: str, info: dict):
        events.push_event(name, info)
        print(f'Pushed event {name} {info}')

    async def save_tweets(self, tweets):
        for tweet in tweets:
            post = TwitterApiStatusPost(tweet).get_post()
            self.db.save_post(post)
            print(f'Saved twitter {tweet}')
            if self.db.check_for_anomaly(post):
                self.loop.create_task(
                    events.push_event(events.POST_ANOMALY,
                                      {'text': post.content}))

    def run(self):
        args = self.parser.parse_args()

        print(f'Retrieving {args.number_of_tweets} tweets')
        tweets_generator = tweet_generator(args.number_of_tweets)

        # Simulates batchs/pages of 15 like Twitter API
        page_size = 15
        for page in range(0, args.number_of_tweets, page_size):
            tweets = itertools.islice(tweets_generator, page_size)
            self.loop.create_task(self.save_tweets(tweets))

        self.loop.run_until_complete(self.loop.shutdown_asyncgens())

        self.loop.close()


def get_tweets():
    GetTweetsCommad().run()


if __name__ == '__main__':
    get_tweets()
