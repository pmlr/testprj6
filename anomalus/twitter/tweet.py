from anomalus.post import Post
from faker import Faker


fake = Faker()


def tweet_generator(n: int) -> dict:
    for i in range(n):
        fake.seed(i)
        yield generate_tweet()


def generate_tweet() -> dict:
    text = fake.text(max_nb_chars=140)
    sample = {
        'username': fake.user_name(),
        'text': text,
        'created_at': fake.date_this_decade(),
    }

    if fake.boolean():
        sample['retweeted_status'] = sample.copy()
        sample['retweeted_status']['username'] = fake.user_name()
        sample['retweeted_status']['created_at'] = fake.date_this_decade()

    return sample


class TwitterApiStatusPost:
    def __init__(self, entry: dict):
        self.entry = entry

    def get_post(self) -> Post:
        if self.entry.get('retweeted_status'):
            original_author = self.entry['retweeted_status']['username']
        else:
            original_author = None

        return Post(content=self.entry['text'],
                    author=self.entry['username'],
                    timestamp=self.entry['created_at'],
                    original_author=original_author)

