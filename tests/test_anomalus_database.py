from datetime import datetime
from unittest import mock, TestCase

from anomalus.database import DB
from anomalus.post import Post


class TestAnomalusDatabase(TestCase):
    CONTENT = 'lorem ipsum'
    AUTHOR = 'paulo'
    TIMESTAMP = datetime(2012, 12, 12)
    ORIGINAL_AUTHOR = 'roberto'
    EDGE = 'repost'

    def test_save_post(self):
        adaptermock = mock.MagicMock()
        db = DB(adaptermock)
        post = Post(content=self.CONTENT,
                    author=self.AUTHOR,
                    timestamp=self.TIMESTAMP)

        db.save_post(post)

        adaptermock.insert_post.assert_called_with(self.CONTENT,
                                                   self.AUTHOR,
                                                   self.TIMESTAMP)

        adaptermock.edge_exists.assert_not_called()

        adaptermock.insert_edge.assert_not_called()

    def test_save_post_with_original_author(self):
        adaptermock = mock.MagicMock(**{'edge_exists.return_value': False})

        db = DB(adaptermock)

        post = Post(content=self.CONTENT,
                    author=self.AUTHOR,
                    original_author=self.ORIGINAL_AUTHOR,
                    timestamp=self.TIMESTAMP)

        db.save_post(post)

        adaptermock.insert_post.assert_called_with(self.CONTENT,
                                                   self.AUTHOR,
                                                   self.TIMESTAMP)

        adaptermock.edge_exists.assert_called()

        adaptermock.insert_edge.assert_called_with(self.EDGE,
                                                   self.AUTHOR,
                                                   self.ORIGINAL_AUTHOR)

    def test_save_post_with_original_author_edge_exists(self):
        adaptermock = mock.MagicMock(**{'edge_exists.return_value': True})

        db = DB(adaptermock)

        post = Post(content=self.CONTENT,
                    author=self.AUTHOR,
                    original_author=self.ORIGINAL_AUTHOR,
                    timestamp=self.TIMESTAMP)

        db.save_post(post)

        adaptermock.insert_post.assert_called_with(self.CONTENT,
                                                   self.AUTHOR,
                                                   self.TIMESTAMP)

        adaptermock.edge_exists.assert_called()

        adaptermock.insert_edge.assert_not_called()
